/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vnu.geoterms.core.Interface;

import javax.swing.JPanel;

/**
 *
 * @author Khanh
 */
public interface IDictionaryEditor {

    JPanel getCreateDictionaryEditor();

    JPanel getEditDictionaryEditor(IDictionary dictionary);

    IDictionary createDictionary();

    void editDictionary(IDictionary dictionary);

}
