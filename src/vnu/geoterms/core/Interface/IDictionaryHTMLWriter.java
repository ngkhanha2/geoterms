/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vnu.geoterms.core.Interface;

/**
 *
 * @author Khanh
 */
public interface IDictionaryHTMLWriter {

    String getHTMLDivContent(IDictionary dictionary, String s);

    String getHTMLContent(IDictionary dictionary, String s);
}
