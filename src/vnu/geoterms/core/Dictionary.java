/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vnu.geoterms.core;

import java.io.File;
import vnu.geoterms.core.Interface.IDictionary;

/**
 *
 * @author Khanh
 */
public abstract class Dictionary implements IDictionary {

    private String path;

    public Dictionary(String path) {
        this.path = path;
    }

    @Override
    public String getPath() {
        return this.path;
    }

}
