/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vnu.geoterms.core;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import vnu.geoterms.core.Interface.*;
import vnu.geoterms.core.structure.spdict.DictionaryFileManagerSPDict;
import vnu.geoterms.core.structure.spdict.DictionaryToolsSPDict;

/**
 *
 * @author Khanh
 */
public class Management implements IManagement {

    private String language;
    private ArrayList<IDictionary> dictionaries = null;
    private ArrayList<IDictionaryTools> dictionaryToolses;

    private String DICTIONARIES_DIRECTORY = System.getProperty("user.dir") + File.separator + "dict";
//    private String CONFIG_FILE = "dict/config.xml";

    public Management() {
        this.dictionaries = new ArrayList<IDictionary>();
        this.dictionaryToolses = new ArrayList<IDictionaryTools>();
        initializeDictionaryToolses();
        loadDictionaries();
    }

    private void initializeDictionaryToolses() {
        this.dictionaryToolses.add(new DictionaryToolsSPDict());
    }

    private void loadDictionaries() {
        File f = new File(DICTIONARIES_DIRECTORY);
        if (!f.exists()) {
            f.mkdir();
            return;
        }
        try {
            File[] files = f.listFiles(new FileFilter() {

                @Override
                public boolean accept(File file) {
                    return file.isDirectory();
                }
            });
            for (int i = 0; i < files.length; ++i) {
                for (int j = 0; j < this.dictionaryToolses.size(); ++j) {
                    String path = this.dictionaryToolses.get(j).getFileManager().findDictionaryPath(files[i].getAbsolutePath());
                    IDictionary dictionary = this.dictionaryToolses.get(j).getFileManager().build(path);
                    if (dictionary != null) {
                        this.dictionaries.add(dictionary);
                    }
                }
            }
        } catch (Exception ex) {

        }
    }

//    private void loadConfiguration() {
//        File f = new File(CONFIG_FILE);
//        if (!f.exists()) {
//            return;
//        }
//        try {
//            Document doc = (Document) (DocumentBuilderFactory.newInstance().newDocumentBuilder()).parse(f);
//            doc.getDocumentElement().normalize();
//            NodeList nodes = doc.getElementsByTagName("dict");
//            for (int i = 0; i < nodes.getLength(); ++i) {
//                Node node = nodes.item(i);
//                if (node.getNodeType() == Node.ELEMENT_NODE) {
//                    Element element = (Element) node;
//                    try {
//                        String dir = element.getAttribute("dir");
//                        DictionarySPDict dict = new DictionarySPDict(dir);
//                        addDictionary(dict);
//                        dict.setSelected(Boolean.parseBoolean(element.getAttribute("selected")));
//                    } catch (Exception ex) {
//
//                    }
//                }
//            }
//        } catch (ParserConfigurationException | SAXException | IOException ex) {
//
//        }
//    }
    @Override
    public ArrayList<IDictionary> getDictionaries() {
        return (ArrayList<IDictionary>) this.dictionaries;
    }

    @Override
    public void addDictionary(IDictionary dictionary) {
        this.dictionaries.add(dictionary);
    }

    @Override
    public void update() {
//        try {
//            Document doc = (Document) (DocumentBuilderFactory.newInstance().newDocumentBuilder()).newDocument();
//            Element dicts = (Element) doc.appendChild(doc.createElement("dicts"));
//            for (int i = 0; i < this.dictionaries.size(); ++i) {
//                Element dict = (Element) dicts.appendChild(doc.createElement("dict"));
//
//                Attr attr = doc.createAttribute("dir");
//                attr.setValue(((Dictionary) this.dictionaries.get(i)).getFileName());
//                dict.setAttributeNode(attr);
//
//                attr = doc.createAttribute("selected");
////                attr.setValue(Boolean.toString(this.dictionaries.get(i).isSelected()));
//                dict.setAttributeNode(attr);
//
//                attr = doc.createAttribute("type");
//                attr.setValue("jspd");
//                dict.setAttributeNode(attr);
//
//                TransformerFactory.newInstance().newTransformer().transform(new DOMSource(doc), new StreamResult(new File(CONFIG_FILE)));
//            }
//        } catch (Exception ex) {
//
//        }
    }

//    @Override
//    public String getCurrentDirectory() {
//        return System.getProperty("user.dir");
//    }
    @Override
    public IDictionaryTools getDictionaryToolsOf(IDictionary dictionary) {
        for (int i = 0; i < this.dictionaryToolses.size(); ++i) {
            if (dictionary.getType().equals(this.dictionaryToolses.get(i).getType())) {
                return this.dictionaryToolses.get(i);
            }
        }
        return null;
    }

    @Override
    public ArrayList<IDictionaryTools> getDictionaryTools() {
        return this.dictionaryToolses;
    }

    @Override
    public ArrayList<String> findEntryInDictionaries(String key) {
        ArrayList<String> list = new ArrayList<>();
        key = key.trim();
        if (!key.isEmpty()) {
            key = key.toLowerCase();
            for (int i = 0; i < this.dictionaries.size(); ++i) {
                int index = this.dictionaries.get(i).find(key);
                if (index == -1) {
                    continue;
                }
                while (true) {
                    String s = this.dictionaries.get(i).getEntry(index).toLowerCase().trim();
                    if (s.startsWith(key)) {
                        list.add(s);
                    } else {
                        break;
                    }
                    ++index;
                }
            }
        }
        return list;
    }

    @Override
    public String getHTMLDefinitionOfEntryInDictionaries(String key) {
        String value = "<html><body><div>";
        int realIndex;
        for (int i = 0; i < this.dictionaries.size(); ++i) {
            realIndex = this.dictionaries.get(i).indexOf(key);
            if (realIndex >= 0) {
                value += "<div><b>" + this.dictionaries.get(i).getName() + "</b><hr></div>";
                for (int j = 0; j < this.dictionaryToolses.size(); ++j) {
                    if (this.dictionaryToolses.get(j).getType().equals(this.dictionaries.get(i).getType())) {
                        value += this.dictionaryToolses.get(j).getHTMLWriter().getHTMLDivContent(this.dictionaries.get(i), this.dictionaries.get(i).getDefinition(realIndex));
                        break;
                    }
                }
                value += "<div></div>";
            }
        }
        value += "</div></body></html>";
        return value;
    }

}
