/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vnu.geoterms.core.structure.spdict;

import java.io.File;
import vnu.geoterms.core.Interface.IDictionary;
import vnu.geoterms.core.Interface.IDictionaryHTMLWriter;

/**
 *
 * @author Khanh
 */
public class DictionaryHTMLWriterSPDict implements IDictionaryHTMLWriter {

    @Override
    public String getHTMLDivContent(IDictionary dictionary, String s) {
        if (s == null) {
            return "<div></div>";
        }
        StringBuilder value = new StringBuilder(s.length() * 2);

        value.append("<div>");
        char kytu = s.charAt(0);
        for (int i = 0; i < s.length(); i++) {
            if ((i == 0) || (s.charAt(i - 1) == '\n')) {
                if (i != 0) {
                    switch (kytu) {
                        case '@':
                            value.append("</font></b><br></div>");
                            break;
                        case '*':
                            value.append("</font></div></b>");
                            break;
                        case '-':
                            value.append("</font></div>");
                            break;
                        case '=':
                            value.append("</font></div>");
                            break;
                        case '+':
                            value.append("</font></div>");
                            break;
                        case '!':
                            value.append("</b></font></div>");
                            break;
                        case '~':
                            value.append("\"></center>");
                            break;
                        default:
                            value.append("</font><br>");
                    }
                }
                switch (s.charAt(i)) {
                    case '@':
                        value.append("<div style=\"margin-left: 0px;\"><font color=red><b>");
                        break;
                    case '*':
                        value.append("<div style=\"margin-left: 20px;\"><font color=blue><b>");
                        break;
                    case '-':
                        value.append("<div style=\"margin-left: 40px;\"><font>");
                        break;
                    case '=':
                        value.append("<div style=\"margin-left: 60px;\"><font color=green>");
                        break;
                    case '+':
                        value.append("<div style=\"margin-left: 60px;\"><font color=gray>");
                        break;
                    case '!':
                        value.append("<div style=\"margin-left: 20px;\"><font color=brown><b>");
                        break;
                    case '~':
                        value.append("<center><img src = \"");
                        File f = new File(dictionary.getDirectory() + File.separator + "images" + File.separator);
                        value.append(f.toURI().toString());
                        break;
                    default:
                        value.append(s.charAt(i));
                }
                kytu = s.charAt(i);
            } else {
                value.append(s.charAt(i));
            }
        }
        switch (kytu) {
            case '@':
                value.append("</font></b><br>");
                break;
            case '*':
                value.append("</div></font></b>");
                break;
            case '-':
                value.append("</font></div>");
                break;
            case '=':
                value.append("</div></font>");
                break;
            case '+':
                value.append("</div></font>");
                break;
            case '!':
                value.append("</b></div></font>");
                break;
            case '~':
                value.append("\"></center>");
                break;
            default:
                value.append("<br>");
        }
        value.append("</div>");

        return value.toString();
    }

    @Override
    public String getHTMLContent(IDictionary dictionary, String s) {
        return "<html><body>" + getHTMLDivContent(dictionary, s) + "</body></html>";
    }

}
