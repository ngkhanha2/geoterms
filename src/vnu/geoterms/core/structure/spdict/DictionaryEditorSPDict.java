/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vnu.geoterms.core.structure.spdict;

import java.io.File;
import java.io.RandomAccessFile;
import javax.swing.JPanel;
import vnu.geoterms.core.DictionaryEditor;
import vnu.geoterms.core.Interface.IDictionary;
import vnu.geoterms.core.structure.spdict.ui.JPanelDictionaryEditortSPDict;

/**
 *
 * @author Khanh
 */
public class DictionaryEditorSPDict extends DictionaryEditor {

    JPanelDictionaryEditortSPDict panel = null;

    private IDictionary create() {
        String fileName = this.panel.getjTextFieldFileName().getText().trim();
        try {
            String dir = this.getDictionariesDirectory() + File.separator + fileName;
            File f = new File(dir);
            if (f.exists() && f.isDirectory()) {
                return null;
            }
            f.mkdir();
            dir = dir + File.separator + fileName + ".jspd";
            f = new File(dir);
            RandomAccessFile raf = new RandomAccessFile(f, "rw");
            raf.setLength(0L);
            raf.write("2SPDict".getBytes("UTF-8"));
            String s = this.panel.getjTextFieldName().getText().trim() + '\000' + this.panel.getjComboBoxLocale().getSelectedItem().toString().substring(0, 2);
            s = s + "\000\000Tahoma\00012\000Tahoma\00012\000" + this.panel.getjTextFieldAuthor().getText().trim() + "\000" + this.panel.getjTextPaneMoreInformation().getText().trim() + " ";
            byte[] bytes = s.getBytes("UTF-8");
            raf.writeInt(15);
            raf.writeInt(0);
            raf.writeShort(bytes.length);
            raf.write(bytes);
            raf.close();
            return new DictionarySPDict(dir);
        } catch (Exception ex) {

        }
        return null;
    }

    @Override
    public JPanel getCreateDictionaryEditor() {
        this.panel = new JPanelDictionaryEditortSPDict();
        return this.panel;
    }

    @Override
    public JPanel getEditDictionaryEditor(IDictionary dictionary) {
        DictionarySPDict dict = (DictionarySPDict) dictionary;
        this.panel = new JPanelDictionaryEditortSPDict();
        this.panel.getjTextFieldFileName().setEditable(false);
        this.panel.getjTextFieldFileName().setText(dict.getFileName());
        this.panel.getjTextFieldName().setEditable(false);
        this.panel.getjTextFieldName().setText(dict.getName());
        this.panel.getjTextFieldAuthor().setEditable(false);
        this.panel.getjTextFieldAuthor().setText(dict.getAuthor());
        this.panel.getjTextPaneMoreInformation().setEditable(false);
        this.panel.getjTextPaneMoreInformation().setText(dict.getMoreInformation());

        this.panel.getjComboBoxLocale().setEditable(false);
        this.panel.getjComboBoxLocale().setSelectedItem(dict.getAuthor());

        return this.panel;
    }

    @Override
    public IDictionary createDictionary() {
        if (this.panel == null) {
            return null;
        }
        if (this.panel.getjTextFieldFileName().isEditable() == false) {
            return null;
        }
        if (this.panel.getjTextFieldFileName().getText().trim().isEmpty()) {

            return null;
        }
        if (this.panel.getjTextFieldName().getText().trim().isEmpty()) {

            return null;
        }
        return create();
    }

    @Override
    public void editDictionary(IDictionary dictionary) {
    }

}
