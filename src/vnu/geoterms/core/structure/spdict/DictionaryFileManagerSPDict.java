/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vnu.geoterms.core.structure.spdict;

import java.io.File;
import java.io.FileFilter;
import java.io.RandomAccessFile;
import vnu.geoterms.core.Interface.IDictionaryFileManager;
import vnu.geoterms.core.Interface.IDictionary;

/**
 *
 * @author Khanh
 */
public class DictionaryFileManagerSPDict implements IDictionaryFileManager {

    @Override
    public IDictionary build(String path) {
        if (check(path)) {
            return new DictionarySPDict(path);
        }
        return null;
    }

    @Override
    public boolean check(String path) {
        File f = new File(path);
        if (!f.exists()) {
            return false;
        }
        try {
            RandomAccessFile raf = new RandomAccessFile(f, "rw");
            byte[] buffer = new byte[7];
            raf.read(buffer, 0, 7);
            String title = new String(buffer, 0, 7, "UTF-8");
            if (title.equals("2SPDict")) {
                raf.close();
                return true;
            }
            raf.close();
        } catch (Exception ex) {

        }
        return false;
    }

    @Override
    public String findDictionaryPath(String directory) {
        File f = new File(directory);
        if (f.isFile()) {
            if (check(directory)) {
                return f.getAbsolutePath();
            }
            return "";
        }
        File[] files = (new File(directory)).listFiles(new FileFilter() {

            @Override
            public boolean accept(File file) {
                return file.isFile();
            }
        });
        for (int i = 0; i < files.length; ++i) {
            if (check(files[i].getAbsolutePath())) {
                return files[i].getAbsolutePath();
            }
        }
        return "";
    }
}
