/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vnu.geoterms.core.structure.spdict;

import vnu.geoterms.core.Interface.IDictionaryEditor;
import vnu.geoterms.core.Interface.IDictionaryFileManager;
import vnu.geoterms.core.Interface.IDictionaryHTMLWriter;
import vnu.geoterms.core.Interface.IDictionaryTools;

/**
 *
 * @author Khanh
 */
public class DictionaryToolsSPDict implements IDictionaryTools {

    private DictionaryEditorSPDict dictionaryEditorSPDict;
    private DictionaryHTMLWriterSPDict dictionaryHTMLWriterSPDict;
    private DictionaryFileManagerSPDict dictionaryFileManagerSPDict;

    public DictionaryToolsSPDict() {
        this.dictionaryEditorSPDict = new DictionaryEditorSPDict();
        this.dictionaryHTMLWriterSPDict = new DictionaryHTMLWriterSPDict();
        this.dictionaryFileManagerSPDict = new DictionaryFileManagerSPDict();
    }

    @Override
    public String getType() {
        return "SPDict";
    }

    @Override
    public IDictionaryHTMLWriter getHTMLWriter() {
        return this.dictionaryHTMLWriterSPDict;
    }

    @Override
    public IDictionaryEditor getEditor() {
        return this.dictionaryEditorSPDict;
    }

    @Override
    public IDictionaryFileManager getFileManager() {
        return this.dictionaryFileManagerSPDict;
    }

}
